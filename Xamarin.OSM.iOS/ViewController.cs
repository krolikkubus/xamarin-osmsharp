﻿using System;
using System.IO;
using System.Threading.Tasks;
using CoreGraphics;
using OsmSharp.iOS.UI;
using OsmSharp.iOS.UI.Controls;
using OsmSharp.Math.Geo;
using OsmSharp.UI.Map.Layers;
using UIKit;

namespace Xamarin.OSM.iOS
{
	public partial class ViewController : UIViewController
	{
		MapView _mapView;
		Layer _mapLayer;
		UIButton _switchOfflineToMbTiles;

		protected ViewController(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			try
			{
				Native.Initialize();

				_mapView = new MapView(View.Frame);
				_mapView.MapCenter = new GeoCoordinate(52.207767, 8.803513);
				_mapView.MapZoom = 12;

				_mapLayer = _mapView.Map.AddLayerTile("http://a.tile.openstreetmap.de/tiles/osmde/{0}/{1}/{2}.png");

				View.AddSubview(_mapView);

				var marker = new MapMarker(new GeoCoordinate(52.207767, 8.803513), MapControlAlignmentType.CenterBottom, UIImage.FromFile("pin.png"));
				_mapView.AddMarker(marker);

				_switchOfflineToMbTiles = new UIButton();
				_switchOfflineToMbTiles.BackgroundColor = UIColor.LightGray;
				_switchOfflineToMbTiles.SetTitle("Switch to MBTiles", UIControlState.Normal);
				_switchOfflineToMbTiles.Frame = new CGRect(10, 30, 150, 30);
				_switchOfflineToMbTiles.TouchUpInside += SwitchOfflineToMbTiles_TouchUpInside;

				View.Add(_switchOfflineToMbTiles);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}

		void SwitchOfflineToMbTiles_TouchUpInside(object sender, EventArgs e)
		{
			Console.WriteLine("Offline init started");

			try
			{
				using (var mapStream = File.OpenRead("./demo_layers.mbtiles"))
				{
					if (_mapLayer != null)
						_mapLayer.Close();

					_mapView.Map.AddLayer(new LayerMBTile(SQLiteConnection.CreateFrom(mapStream, "map")));
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}

			Console.WriteLine("Offline init finished");
		}
	}
}

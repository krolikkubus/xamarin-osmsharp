﻿using System;
using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using OsmSharp.Android.UI;
using OsmSharp.Android.UI.Data.SQLite;
using OsmSharp.Math.Geo;
using OsmSharp.UI.Map;
using OsmSharp.UI.Map.Layers;

namespace Xamarin.OSM.Droid
{
    [Activity(Label = "Xamarin.OSM.Droid", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        MapView _mapView;
        Layer _mapLayer;
        Button _switchOfflineToMbTiles;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Native.Initialize();

            _mapView = new MapView(this, new MapViewSurface(this));
            _mapView.MapTilt = 0; // must be set otherwise NullReferenceException
            _mapView.MapCenter = new GeoCoordinate(52.207767, 8.803513);
            _mapView.MapZoom = 12;
            _mapView.Map = new Map();
            _mapView.MapAllowZoom = true;
            _mapLayer = _mapView.Map.AddLayerTile("http://a.tile.openstreetmap.de/tiles/osmde/{0}/{1}/{2}.png");

            using (var bitmap = BitmapFactory.DecodeResource(Resources, Resource.Drawable.pin))
            {
                var marker = new MapMarker(this, new GeoCoordinate(52.207767, 8.803513), MapMarkerAlignmentType.CenterBottom, bitmap);
                _mapView.AddMarker(marker);
            }

            _switchOfflineToMbTiles = new Button(this);
            _switchOfflineToMbTiles.Text = "Switch to MBTiles";
            _switchOfflineToMbTiles.Click += SwitchOfflineToMbTiles_Click;

            var layout = new RelativeLayout(this);
            layout.AddView(_mapView);
            layout.AddView(_switchOfflineToMbTiles);

            SetContentView(layout);
        }

        void SwitchOfflineToMbTiles_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Offline init started");

            try
            {
                using (var mapStream = Resources.Assets.Open("demo_layers.mbtiles"))
                {
                    if (_mapLayer != null)
                        _mapView.Map.RemoveLayer(_mapLayer);

                    _mapView.Map.AddLayer(new LayerMBTile(SQLiteConnection.CreateFrom(mapStream, "map")));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.WriteLine("Offline init finished");
        }
    }
}

